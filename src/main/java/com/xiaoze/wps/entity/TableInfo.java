package com.xiaoze.wps.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 表信息
 * @author smirk小泽
 * @date 2018/9/11
 */
public class TableInfo implements Serializable {

	private static final long serialVersionUID = -4325866163514519482L;

	/**
	 * 数据库名
	 */
	private String tableSchema;

	/**
	 * 表名
	 */
	private String tableName;

	/**
	 * 数据库引擎名称
	 */
	private String engine;

	/**
	 * 表新建时间
	 */
	private String createTime;

	/**
	 * 备注信息
	 */
	private String tableComment;

	/**
	 * 列信息
	 */
	private List<TableFieldInfo> tableFieldInfoList;

	/**
	 * 表的索引信息
	 */
	private List<TableKeyInfo> tableKeyInfoList;

	public String getTableSchema() {
		return tableSchema;
	}

	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	public List<TableFieldInfo> getTableFieldInfoList() {
		return tableFieldInfoList;
	}

	public void setTableFieldInfoList(List<TableFieldInfo> tableFieldInfoList) {
		this.tableFieldInfoList = tableFieldInfoList;
	}

	public List<TableKeyInfo> getTableKeyInfoList() {
		return tableKeyInfoList;
	}

	public void setTableKeyInfoList(List<TableKeyInfo> tableKeyInfoList) {
		this.tableKeyInfoList = tableKeyInfoList;
	}
}
