package com.xiaoze.wps.entity;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 表的索引信息
 * @author smirk小泽
 * @date 2018/9/11
 */
public class TableKeyInfo implements Serializable {

	private static final long serialVersionUID = 8674526228124842524L;

	/**
	 * 表名称
	 */
	private String table;

	/**
	 * 索引名称
	 */
	private String keyName;

	/**
	 * 索引的原字段名称
	 */
	private String columnName;

	/**
	 * 是否唯一
	 */
	private String nonUnique;

	/**
	 * 是否唯一索引名称
	 */
	private String uniqueName;

	/**
	 * 索引类型
	 */
	private String indexType;

	/**
	 * 索引注释
	 */
	private String indexComment;

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getNonUnique() {
		return nonUnique;
	}

	public void setNonUnique(String nonUnique) {

		if("0".equals(nonUnique)){
			this.uniqueName = "UNIQUE";
		}else{
			this.uniqueName = "NON-UNIQUE";
		}
		this.nonUnique = nonUnique;
	}

	public String getIndexType() {
		return indexType;
	}

	public void setIndexType(String indexType) {
		this.indexType = indexType;
	}

	public String getIndexComment() {
		return indexComment;
	}

	public void setIndexComment(String indexComment) {
		this.indexComment = indexComment;
	}

	public String getUniqueName() {
		return uniqueName;
	}

	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}
}
