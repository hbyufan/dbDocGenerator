package com.xiaoze.wps.entity;

import java.io.Serializable;

/**
 * 列信息
 * @author smirk小泽
 * @date 2018/9/11
 */
public class TableFieldInfo implements Serializable {

	private static final long serialVersionUID = 1331119044626626786L;

	/***
	 * 列名
	 */
	private String columnName;

	/***
	 * 类型
	 */
	private String columnType;

	/***
	 * 是否能为空
	 */
	private String isNullAble;

	/***
	 * 键
	 */
	private String columnKey;

	/***
	 * 默认值
	 */
	private String columnDefault;

	/***
	 * 额外信息
	 */
	private String extra;

	/***
	 * 备注信息
	 */
	private String columnComment;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getIsNullAble() {
		return isNullAble;
	}

	public void setIsNullAble(String isNullAble) {
		this.isNullAble = isNullAble;
	}

	public String getColumnKey() {
		return columnKey;
	}

	public void setColumnKey(String columnKey) {
		this.columnKey = columnKey;
	}

	public String getColumnDefault() {
		return columnDefault;
	}

	public void setColumnDefault(String columnDefault) {
		this.columnDefault = columnDefault;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getColumnComment() {
		return columnComment;
	}

	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}

	@Override
	public String toString() {
		return "TableFieldInfo{" +
				"columnName='" + columnName + '\'' +
				", columnType='" + columnType + '\'' +
				", isNullAble='" + isNullAble + '\'' +
				", columnKey='" + columnKey + '\'' +
				", columnDefault='" + columnDefault + '\'' +
				", extra='" + extra + '\'' +
				", columnComment='" + columnComment + '\'' +
				'}';
	}
}
