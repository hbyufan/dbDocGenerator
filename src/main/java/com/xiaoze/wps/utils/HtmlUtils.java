package com.xiaoze.wps.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * HtmlUtils
 * @author smirk小泽
 * @date 2018/9/11
 */
public class HtmlUtils {

    private static final Logger logger = LoggerFactory.getLogger(HtmlUtils.class);

    public void createHtml(ClassLoaderTemplateResolver resolver, Context context, String orginHtml, String dirPath, String fileName) {

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(resolver);

        try {
            // 生成文件夹
            File dir = new File(dirPath);
            FileUtils.forceMkdir(dir);

            //渲染模板
            FileWriter write = new FileWriter(dirPath + fileName);
            templateEngine.process(orginHtml, context, write);

            logger.info("已在(" + dirPath + fileName + ")生成静态网页");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
