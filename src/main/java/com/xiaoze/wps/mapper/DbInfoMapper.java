package com.xiaoze.wps.mapper;

import java.util.List;

import com.xiaoze.wps.entity.TableFieldInfo;
import com.xiaoze.wps.entity.TableInfo;
import com.xiaoze.wps.entity.TableKeyInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 获取数据库信息的mapper
 * @author smirk小泽
 * @date 2018/9/11
 */
public interface DbInfoMapper {
	
	/**
	 * 获取数据库名称
	 * @return String
	 */
	List<TableInfo> getTableInfo();
	
	/**
	 * 获取列信息
	 * @return List<TableFieldInfo>
	 */
	List<TableFieldInfo> getTableFieldInfo(String tableName);

	/**
	 * 获取索引信息
	 * @return List<TableKeyInfo>
	 */
	List<TableKeyInfo> getTableKeyInfo(@Param("tableName")String tableName);
	
}
