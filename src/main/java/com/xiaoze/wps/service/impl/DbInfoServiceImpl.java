package com.xiaoze.wps.service.impl;

import com.xiaoze.wps.entity.TableFieldInfo;
import com.xiaoze.wps.entity.TableInfo;
import com.xiaoze.wps.entity.TableKeyInfo;
import com.xiaoze.wps.mapper.DbInfoMapper;
import com.xiaoze.wps.service.DbInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * DbInfoServiceImpl
 * @author smirk小泽
 * @date 2018/9/11
 */
@Service
public class DbInfoServiceImpl implements DbInfoService {

    @Autowired
    DbInfoMapper dbInfoMapper;

    @Override
    public List<TableInfo> getTableInfo() {
        return dbInfoMapper.getTableInfo();
    }

    @Override
    public List<TableFieldInfo> getTableFieldInfo(String tableName) {
        return dbInfoMapper.getTableFieldInfo(tableName);
    }

    @Override
    public List<TableKeyInfo> getTableKeyInfo(String tableName) {
        return dbInfoMapper.getTableKeyInfo(tableName);
    }
}
