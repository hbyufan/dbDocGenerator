package com.xiaoze.wps.service;

import com.xiaoze.wps.entity.TableFieldInfo;
import com.xiaoze.wps.entity.TableInfo;
import com.xiaoze.wps.entity.TableKeyInfo;

import java.util.List;

/**
 * DbInfoService
 * @author smirk小泽
 * @date 2018/9/11
 */
public interface DbInfoService {

    /**
     * 获取数据库名称
     * @return String
     */
    List<TableInfo> getTableInfo();

    /**
     * 获取列信息
     * @return List<TableFieldInfo>
     */
    List<TableFieldInfo> getTableFieldInfo(String tableName);

    /**
     * 获取索引信息
     * @return List<TableKeyInfo>
     */
    List<TableKeyInfo> getTableKeyInfo(String tableName);

}
