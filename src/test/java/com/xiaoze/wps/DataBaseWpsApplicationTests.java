package com.xiaoze.wps;

import com.xiaoze.wps.entity.TableFieldInfo;
import com.xiaoze.wps.entity.TableInfo;
import com.xiaoze.wps.entity.TableKeyInfo;
import com.xiaoze.wps.service.DbInfoService;
import com.xiaoze.wps.utils.ExcelUtils;
import com.xiaoze.wps.utils.HtmlUtils;
import com.xiaoze.wps.utils.WordUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataBaseWpsApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(HtmlUtils.class);

    private static String[] tableInfoHead = {"序号", "表名称", "注释"};
    private static String[] tableFieldInfoHead = {"字段", "注释", "类型", "键","能否为空", "默认值", "其他"};
    private static String[] tableKeyInfoHead = {"名称", "栏位", "索引类型", "索引方式", "索引备注"};

    /**
     * 数据主表
     */
    private static Long[] tableInfoColumnWidth = {2500L, 2500L, 2500L};

    /**
     * 字段信息列宽
     */
    private static Long[] tableFieldInfoColumnWidth = {1200L, 2500L, 1300L, 800L, 800L, 1000L, 1500L};

    /**
     * 索引信息列宽
     */
    private static Long[] tableKeyInfoColumnWidth = {1500L, 1500L, 1500L, 1500L, 1500L};

    @Autowired
    DbInfoService dbInfoService;

    @Value("${application.generator.target-file-dir}")
    private String targetFileDir;

    @Test
    public void testHtml() {

        List<TableInfo> tableInfoList = dbInfoService.getTableInfo();

        //处理TableInfoList
        getAllTableInfoList(tableInfoList);

       //构造模板引擎
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        resolver.setPrefix("templates/");//模板所在目录，相对于当前classloader的classpath。
        resolver.setSuffix(".html");//模板文件后缀

        //构造上下文(Model)
        Context context = new Context();
        context.setVariable("tableInfoList", tableInfoList);
        context.setVariable("dataBaseName", tableInfoList.get(0).getTableSchema());

        //原始网页名字(就是模板网页名字)
        String orginHtml = "htmlTemplate";
        Random random = new Random();

        //生成静态网页的名字
        String newHtml = DateFormatUtils.format(new Date(), "yyyy-MM-dd_hh-mm-ss") + random.nextInt(10) + ".html";

        HtmlUtils htmlUtils = new HtmlUtils();
        htmlUtils.createHtml(resolver, context, orginHtml, targetFileDir, newHtml);

    }

    @Test
    public void testExcel() throws IOException {

        List<TableInfo> tableInfoList = dbInfoService.getTableInfo();

        //处理TableInfoList
        getAllTableInfoList(tableInfoList);

        ExcelUtils excelUtils = new ExcelUtils();
        excelUtils.createExcel(tableInfoList, targetFileDir);

    }

    @Test
    public void testWord() throws Exception {

        List<TableInfo> tableInfoList = dbInfoService.getTableInfo();

        //处理TableInfoList
        getAllTableInfoList(tableInfoList);

        XWPFDocument docxDocument = new XWPFDocument();
        WordUtil.addCustomHeadingStyle(docxDocument, "title1", 1);
        WordUtil.addCustomHeadingStyle(docxDocument, "title2", 2);
        WordUtil.addContentTitle(docxDocument,"title1","一、数据总表");

        //添加数据库总表信息
        int tableSize = tableInfoList.size();
        XWPFTable table = WordUtil.addTable(docxDocument, tableSize + 1, 3, tableInfoColumnWidth);
        List<XWPFTableRow> rows = table.getRows();
        for (int i=0; i< rows.size(); i++) {
            XWPFTableRow row = rows.get(i);
            List<XWPFTableCell> cells = row.getTableCells();
            TableInfo tableInfo = null;
            if(i!=0){
                tableInfo = tableInfoList.get(i - 1);
            }
            for (int j=0; j<cells.size(); j++) {
                XWPFTableCell cell = cells.get(j);
                if(i==0){
                    cell.setText(tableInfoHead[j]);
                }else{
                    switch (j){
                        case 0:
                            cell.setText(String.valueOf(i));
                            break;
                        case 1:
                            cell.setText(tableInfo.getTableName());
                            break;
                        case 2:
                            cell.setText(tableInfo.getTableComment());
                            break;
                        default:
                            logger.error("undefined column:" + (j + 1));
                    }
                }

            }
        }
        ///以下操作空一行
        WordUtil.changeLine(docxDocument, 1);

        //数据库表的详细开始一个一级标题，每个表都是一个二级标题
        WordUtil.addContentTitle(docxDocument,"title1","二、各表详细信息");
        for(int k=0; k<tableInfoList.size(); k++){
            if(k != 0){
                //以下操作空一行
                WordUtil.changeLine(docxDocument, 1);
            }

            TableInfo tableInfo = tableInfoList.get(k);
            String tableNameDetail;
            if(StringUtils.isNotBlank(tableInfo.getTableComment())){
                tableNameDetail = (k + 1) + "、"+ tableInfo.getTableName() + " (" + tableInfo.getTableComment() + ")";
            }else{
                tableNameDetail = (k + 1) + "、"+ tableInfo.getTableName();
            }

            WordUtil.addContentTitle(docxDocument,"title2",tableNameDetail);

            WordUtil.addText(docxDocument, 2, "表格信息", true);

            //添加表详细信息
            List<TableFieldInfo> tableFieldInfoList = tableInfo.getTableFieldInfoList();
            if((tableFieldInfoList != null) && (!tableFieldInfoList.isEmpty())){
                createTableFieldInfo(docxDocument, tableFieldInfoList);
            }

            ///以下操作空一行
            WordUtil.changeLine(docxDocument, 1);

            WordUtil.addText(docxDocument, 2, "索引信息", true);

            //添加索引详细信息
            List<TableKeyInfo> tableKeyInfoList = tableInfo.getTableKeyInfoList();

            if((tableKeyInfoList != null) && (!tableKeyInfoList.isEmpty())){
                createTableKeyInfo(docxDocument, tableKeyInfoList);
            }
        }

        String dataBaseName = tableInfoList.get(0).getTableSchema();
        Random random = new Random();
        String filename = dataBaseName + "_" + DateFormatUtils.format(new Date(), "yyyy-MM-dd_HH-mm-ss") + random.nextInt(10) + ".docx";

        // 生成文件夹
        File dir = new File(targetFileDir);
        FileUtils.forceMkdir(dir);
        // word写入到文件
        WordUtil.saveDocument(docxDocument,targetFileDir + filename);
        logger.info("已在(" + targetFileDir + filename + ")生成Word");

    }

    private void getAllTableInfoList(List<TableInfo> tableInfoList){
        tableInfoList.forEach((tableInfo) -> {

            logger.info(tableInfo.getTableName());

            List<TableFieldInfo> tableFieldInfoList = dbInfoService.getTableFieldInfo(tableInfo.getTableName());
            if(!(tableFieldInfoList.isEmpty())){
                tableInfo.setTableFieldInfoList(tableFieldInfoList);
            }

            List<TableKeyInfo> tableKeyInfoList = dbInfoService.getTableKeyInfo(tableInfo.getTableName());
            if(!(tableKeyInfoList.isEmpty())){
                tableInfo.setTableKeyInfoList(tableKeyInfoList);
            }
        });
    }

    private void createTableFieldInfo(XWPFDocument docxDocument, List<TableFieldInfo> tableFieldInfoList){
        int colSize = tableFieldInfoList.size();
        XWPFTable tableDetail = WordUtil.addTable(docxDocument, colSize + 1, 7, tableFieldInfoColumnWidth);
        List<XWPFTableRow> tableRows = tableDetail.getRows();
        for (int i=0; i< tableRows.size(); i++) {
            XWPFTableRow row = tableRows.get(i);
            List<XWPFTableCell> cells = row.getTableCells();
            TableFieldInfo tableFieldInfo = null;
            if(i!=0){
                tableFieldInfo = tableFieldInfoList.get(i - 1);
            }
            for (int j=0; j<cells.size(); j++) {
                XWPFTableCell cell = cells.get(j);
                if(i==0){
                    cell.setText(tableFieldInfoHead[j]);
                }else{
                    switch (j){
                        case 0:
                            cell.setText(tableFieldInfo.getColumnName());
                            break;
                        case 1:
                            cell.setText(tableFieldInfo.getColumnComment());
                            break;
                        case 2:
                            cell.setText(tableFieldInfo.getColumnType());
                            break;
                        case 3:
                            cell.setText(tableFieldInfo.getColumnKey());
                            break;
                        case 4:
                            cell.setText(tableFieldInfo.getIsNullAble());
                            break;
                        case 5:
                            cell.setText(tableFieldInfo.getColumnDefault());
                            break;
                        case 6:
                            cell.setText(tableFieldInfo.getExtra());
                            break;
                        default:
                            logger.error("undefined column:" + (j + 1));
                    }
                }

            }
        }
    }

    private void createTableKeyInfo(XWPFDocument docxDocument, List<TableKeyInfo> tableKeyInfoList){
        int colSize = tableKeyInfoList.size();
        XWPFTable tableDetail = WordUtil.addTable(docxDocument, colSize + 1, 5, tableKeyInfoColumnWidth);
        List<XWPFTableRow> tableRows = tableDetail.getRows();
        for (int i=0; i< tableRows.size(); i++) {
            XWPFTableRow row = tableRows.get(i);
            List<XWPFTableCell> cells = row.getTableCells();
            TableKeyInfo tableKeyInfo = null;
            if(i!=0){
                tableKeyInfo = tableKeyInfoList.get(i - 1);
            }
            for (int j=0; j<cells.size(); j++) {
                XWPFTableCell cell = cells.get(j);
                if(i==0){
                    cell.setText(tableKeyInfoHead[j]);
                }else{
                    switch (j){
                        case 0:
                            cell.setText(tableKeyInfo.getKeyName());
                            break;
                        case 1:
                            cell.setText(tableKeyInfo.getColumnName());
                            break;
                        case 2:
                            cell.setText(tableKeyInfo.getUniqueName());
                            break;
                        case 3:
                            cell.setText(tableKeyInfo.getIndexType());
                            break;
                        case 4:
                            cell.setText(tableKeyInfo.getIndexComment());
                            break;
                        default:
                            logger.error("undefined column:" + (j + 1));
                    }
                }

            }
        }
    }

}
